package com.cg.altran.assignment6;




import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * This is a ticket machine test class to check the test cases.
 * 
 * @see TicketMachine
 * @author Nithish Kumar K
 * @version 1.0
 * @since 15-Jan-2021
 *
 */

public class TicketMachineTest {
	private static TicketMachine firstObj = null; // Declaring the TicketMachine object and initializing the vlaue to
	// null.
	private static TicketMachine secondObj = null; // Declaring the TicketMachine object and initializing the vlaue to
	// null.

	/**
	 * This is a before class which is executed before the test cases.
	 * 
	 * @see setUpBeforeClass
	 */
	@BeforeClass
	public static void setUpBeforeClass() {
		firstObj = new TicketMachine(110, 10);
		secondObj = new TicketMachine(111, 5);
	}

	/**
	 * This is an after class which where the value after performing the test cases
	 * is destroyed. And it is executed after performing all the test cases.
	 * 
	 * @see setUpAfterClass
	 */
	@AfterClass
	public static void setUpAfterClass() {
		firstObj = null; // Assigning null after performing all the test cases to firstObj.
		secondObj = null; // Assigning null after performing all the test cases to secondObj.
	}

	/**
	 * This is a test case to check the function testAddMoneyAndNoOfTicketsReq().
	 * And checks the expected value with the actual value.
	 * 
	 * @see testAddMoneyAndNoOfTicketsReq
	 */
	@Test
	public void testAddMoneyAndNoOfTicketsReq() {
		// Comparing the expected String value with the actual value.
		 assertEquals("Value added successfully",firstObj.adddMoneyAndNoOfTicketsReq(150,4));
		// Checking the actual money paid and tickets required is more than the assigned
		// value.
	}

	/**
	 * This is a test case to check the function testAddMoneyAndNoOfTicketsReq().
	 * And checks the expected value with the actual value.
	 * 
	 * @see testAddMoneyAndNoOfTicketsReq
	 */
	@Test
	public void testAddMoneyAndNoOfTicketsReqisinvalid() {
		// Comparing the expected String value with the actual value.
		 assertEquals("Value is too less to add",secondObj.adddMoneyAndNoOfTicketsReq(-20, 7));
		// Checking the actual money paid and tickets required is more than the assigned
		// value.
	}

	

	/**
	 * This is a test case to check the function testCheckTicketAvailability(). And
	 * checks the expected value with the actual value.
	 * 
	 * @see
	 * 
	 */
	@Test
	public void testCheckTicketAvailability() {
		firstObj.adddMoneyAndNoOfTicketsReq(150, 4);
		// Comparing the expected boolean value to the actual value.
		 assertEquals(true, firstObj.checkTicketAvailability());
	}

	/**
	 * This is a test case to check the function testCheckTicketAvailability(). And
	 * checks the expected value with the actual value.
	 * 
	 * @see
	 * 
	 */
	/*
	 * @Test public void testCheckTicketAvailabilityisless() {
	 * secondObj.addMoneyAndNoOfTicketsReq(-20,7); // Comparing the expected boolean
	 * value to the actual value. assertEquals(false,
	 * secondObj.checkTicketAvailability()); }
	 */

	/**
	 * This is a test case to check the function testPrintTicket(). And checks the
	 * expected value with the actual value.
	 * 
	 * @see testPrintTicket
	 */
	@Test
	public void testPrintTicket() {
		firstObj.adddMoneyAndNoOfTicketsReq(150, 4);
		// Checking whether the user has paid extra amount of money and tickets
		// requested.
	
		 assertEquals("Ticket is printed and extra amount will be refunded to the user",firstObj.printTicket());
		// Comparing the expected String value to the actual value.
	}

	/**
	 * This is a test case to check the function testPrintTicket(). And checks the
	 * expected value with the actual value.
	 * 
	 * @see testPrintTicket
	 */
	@Test
	public void testPrintTicketIsCancelled() {
		secondObj.adddMoneyAndNoOfTicketsReq(-20, 7);
		// Checking whether the user has paid extra amount of money and tickets
		// requested.
		 assertEquals("Remaining amount of money needs to be inserted to purchase the ticket", secondObj.printTicket());
		// Comparing the expected String value to the actual value.
	}

}
