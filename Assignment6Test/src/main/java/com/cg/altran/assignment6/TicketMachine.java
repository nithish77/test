/*****************************************************
                         Altran Technologies Proprietary

This source code is the sole property of Altran Technologies. Any form of utilization
of this source code in whole or in part is  prohibited without  written consent from
Altran Technologies

	  File Name	           	 	:TickectMachine.java
	  Project Name				:Assignment6
	  Package Name				:com.cg.altran.assignment6
	  Principal Author      	:Nithish Kumar K
	  Subsystem Name        	:
	  Module Name           	:
	  Date of First Release 	:2022-01-15
	  Author					:Nithish Kumar K
	  Description           	: Write an application to create a Ticket Machine and test the same by calling various functions and Test the application using JUNIT.


	  Change History

	  Version      			: 1.0
	  Date(DD/MM/YYYY) 		:2022-01-15
	  Modified by			: 
	  Description of change :

***********************************************************************/

package com.cg.altran.assignment6;

public class TicketMachine {
	private int noOfTicketsAvail;
	private int noOfTicketRequested;
	private int ticketPrice;
	private int money;
	
	/**
	 *        Creating the constructor and performing validation of ticket and number of tickets available.
	 * @param ticketPrice      The price of the ticket.
	 * @param noOfTicketsAvail     The number of tickets available
	 */
	public TicketMachine(int ticketPrice, int noOfTicketsAvail) {
		if ((ticketPrice> 0) || (noOfTicketsAvail > 0)) {
			this.ticketPrice = ticketPrice;
			this.noOfTicketsAvail = noOfTicketsAvail;
		} 
		else { 
			System.out.println("Invalid value enter. The value entered should be greater than 0.");
		}
	}
	

	/**
	 *           Adding the money and ticket requested to the default value.
	 * @param money       The money entered by the user.
	 * @param noOfTicketRequested    Number of tickets requested.
	 */
	
	public String adddMoneyAndNoOfTicketsReq(int money,int noOfTicketRequested) {
		if (money < 1 || noOfTicketRequested < 1) {
			return ("Value is too less to add");
		} else {
			this.money = this.money + money;
			this.noOfTicketRequested = noOfTicketRequested;
			return ("Value added successfully");
		}
	}
	

	 /**
	  *            Creating the method to check the availability of ticket.
	  * @return  Checks the condition whether available ticket is greater than requested ticket.
	  */
	public boolean checkTicketAvailability() {
		if (noOfTicketsAvail < noOfTicketRequested) {
			return false;
		} else {
			noOfTicketsAvail = noOfTicketsAvail - noOfTicketRequested;
			return true;
		}
	}
	
    /**
     *           Printing the tickets after checking the conditions.
     */
	public String printTicket() {
		if (this.money < ticketPrice) {
			return ("Remaining amount of money needs to be inserted to purchase the ticket");
		} else {
			return ("Ticket is printed and extra amount will be refunded to the user");
		}
		
	}
	
	/**
	 *              Displaying the final details.
	 */
	public void displayDetails() {
		System.out.println("Ticket available are: " + noOfTicketsAvail );
		System.out.println("Ticket price are: " + ticketPrice);
		System.out.println("Ticket requested are: " + noOfTicketRequested);
		System.out.println("Amount paid are: " + money);
	}

}
