/*****************************************************
                         Altran Technologies Proprietary

This source code is the sole property of Altran Technologies. Any form of utilization
of this source code in whole or in part is  prohibited without  written consent from
Altran Technologies

	  File Name	           	 	:TestMachine.java
	  Project Name				:Assignment6
	  Package Name				:com.cg.altran.assignment6
	  Principal Author      	:Nithish Kumar K
	  Subsystem Name        	:
	  Module Name           	:
	  Date of First Release 	:2022-01-15
	  Author					:Nithish Kumar K
	  Description           	: Write an application to create a Ticket Machine and test the same by calling various functions and Test the application using JUNIT.


	  Change History

	  Version      			: 1.0
	  Date(DD/MM/YYYY) 		:2022-01-15
	  Modified by			: 
	  Description of change :

***********************************************************************/

package com.cg.altran.assignment6;

import java.util.Scanner;

public class TestMachine {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in); // Scanner to scan the user input.

		System.out.println("Enter the tickets Available: ");
		int noOfTicketsAvail = scan.nextInt(); // Scanning the tickets available.
		System.out.println("Enter the ticket price");
		int ticketPrice = scan.nextInt(); // scanning the ticket price
		TicketMachine ticket = new TicketMachine(ticketPrice, noOfTicketsAvail); // creating the class object and
																					// calling the function
		System.out.println("Enter the number of tickets required: ");
		int noOfTicketRequested = scan.nextInt(); // scanning the number of tickets required.
		System.out.println("Enter the amount of money to buy the ticket.");
		int money = scan.nextInt(); // Scanning the money entered by the user.
		ticket.adddMoneyAndNoOfTicketsReq(money, noOfTicketRequested); // Calling the add money and no of ticket
																		// required function.
		if (ticket.checkTicketAvailability() == false) {
			System.out.println("Number of Ticket Requested is more than number of ticket available.");
			System.exit(0);
		} else {
			ticket.printTicket();
		}
		ticket.displayDetails();

	}

}
